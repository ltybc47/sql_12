﻿using Domain;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlazorApp2.Data
{
    public class ObjectService<T> where T : BaseObject
    {
        private readonly ProjectDbContext _db;
        private readonly IDatabase redis;
        private readonly ILogger<T> logger;

        public ObjectService(ProjectDbContext db, IDatabase redis, ILogger<T> logger)
        {
            _db = db;
            this.redis = redis;
            this.logger = logger;
        }
        public List<T> listObjects = new List<T>();

        public List<T> ListObjects
        {
            get
            {
                if (listObjects == null)
                    ListObjects = GetListObjects().Result;
                return ListObjects;
            }
            set
            {
                listObjects = value;
            }
        }

        public async Task<List<T>> GetListObjects()
        {
            return await _db.Set<T>().ToListAsync();
        }
        public List<T> GetListObjects<T>(Func<T, bool> foo)
        {
            return _db.Set<T>().Where(foo).ToList();
        }
        public async Task Create(T obj)
        {
            _ = _db.AddAsync<T>(obj);
            listObjects.Add(obj);
            await _db.SaveChangesAsync();
        }
        public async Task<T> GetById(string ID)
        {
            T t;
            //t = await _db.Set<T>().Where(x => x.Id.ToString() == ID).FirstOrDefaultAsync();
            t = await GetTFromCache(ID);
            if (t is null)
            {
                t = await _db.Set<T>().Where(x => x.Id.ToString() == ID).FirstOrDefaultAsync();
                _ = AddTToCache(t);
            }
            else
            {

            }
            return t;
        }


        private async Task AddTToCache(T t)
        {
            if (t != null)
            {
                var redisKey = GetCacheKey(t.GetId().ToString());
                await redis.StringSetAsync(redisKey, JsonSerializer.Serialize(t), flags: CommandFlags.FireAndForget);
            }
        }
        private async Task UpdateTToCache(T t)
        {
            var redisKey = GetCacheKey(t.GetId().ToString());
            redis.KeyDelete(redisKey);
            await AddTToCache(t);
        }


        private async Task<T> GetTFromCache(string iD)
        {
            var redisKey = GetCacheKey(iD);
            string cache = await redis.StringGetAsync(redisKey);

            if (!string.IsNullOrEmpty(cache))
            {
                try
                {
                    return JsonSerializer.Deserialize<T>(cache);
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Could not deserialize user");
                }
            }

            return null;
        }


        private static string GetCacheKey(string id)
        {
            return $"object:id_{id}";
        }

        public async Task<string> Update(T obj)
        {
            var dbObject = await _db.Set<T>().Where(x => x.Id.ToString() == obj.Id.ToString()).FirstOrDefaultAsync();
            if (dbObject != null)
            {
                dbObject.UpdatePropertys(obj);
                _db.Update<T>(dbObject);
                _db.SaveChanges();
                return await Task.FromResult("ok");
            }
            else
            {

                return await Task.FromResult("object is deleted");
            }
            //_ = UpdateTToCache(obj);
        }
        public async Task Delete(T obj)
        {
            _db.Remove<T>(obj);
            await _db.SaveChangesAsync();
        }
    }
}
