﻿using BlazorApp2.Data;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp2.Services
{
  public interface IUserService
  {
    public Task<Client> LoginAsync(Client user);
    public Task<Client> RegisterUserAsync(Client user);
    public Task<Client> GetUserByAccessTokenAsync(string accessToken);
    public Task<Client> RefreshTokenAsync(RefreshRequest refreshRequest);
  }
}
