﻿using Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp2
{
  public static class GlobalVars
  {
    public static IConfigurationRoot config = InitConfiguration();
    public static ProjectDbContext db = DbContextExtentions.GetDbContext(config.GetConnectionString("DockerMssqlConnection"));

    private static IConfigurationRoot InitConfiguration()
    {
      //      Установите 2 пакета
      //Microsoft.Extensions.Configuration.FileExtensions
      //Microsoft.Extensions.Configuration.Json

      var builder = new ConfigurationBuilder();
      // установка пути к текущему каталогу
      string baseDirectory = Directory.GetCurrentDirectory();
      builder.SetBasePath(baseDirectory);

      // получаем конфигурацию из файла appsettings.json
      builder.AddJsonFile("appsettings.json");
      // создаем конфигурацию
      IConfigurationRoot config = builder.Build();
      return config;
    }
  }
}
