﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
  public class Category : BaseObject
  {
    public string Name { get; set; }
    public Category ParentCategory { get; set; }
    [Display(Name="Категория", Description ="К какой компании относится эта категория")]
    public Company Company { get; set; }
    //public Category[] ChildsCategory { get; set; }
    public Category()
    {

    }
    public Category(Company company)
    {
      Company = company;
    }

        public override void UpdatePropertys(BaseObject obj)
        {
            this.Name = (obj as Category).Name;
            this.ParentCategory = (obj as Category).ParentCategory;
            this.Company= (obj as Category).Company;
        }
    }
}
