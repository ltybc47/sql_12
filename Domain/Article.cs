﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Article : BaseObject
    {
        public string Name { get; set; }
        public string Description{ get; set; }
        public DateTime Datetime { get; set; }
        public string Text{ get; set; }
        public string ImagePath{ get; set; }
        public Company  ParentCompany { get; set; }
        public Client  Author{ get; set; }

        public override void UpdatePropertys(BaseObject obj)
        {
            this.Name = (obj as Article).Name;
            this.Description = (obj as Article).Description;
            this.Datetime= (obj as Article).Datetime;
            this.Text= (obj as Article).Text;
            this.ImagePath = (obj as Article).ImagePath;
            this.ParentCompany= (obj as Article).ParentCompany;
            this.Author= (obj as Article).Author;
        }
    }
}
