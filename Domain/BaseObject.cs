﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
  public abstract class BaseObject : IBaseObject<BaseObject>
  {
    public int Id { get; set; }

    public int GetId()
    {
      return Id;
    }

        public abstract void UpdatePropertys(BaseObject obj);
    }
}
