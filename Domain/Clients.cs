﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Client : BaseObject
    {
        [Display(Name = "Логин")]
        public string Name { get; set; }
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        [Display(Name = "Почта")]
        public string Email { get; set; }

        [Display(Name = "Список компаний пользователя")]
        public List<Company> Companys { get; set; }


        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public Role Role { get; set; }

        public override void UpdatePropertys(BaseObject obj)
        {
            this.Name = (obj as Client).Name;
            this.Password = (obj as Client).Password;
        }
    }
}
