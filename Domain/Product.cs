﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Domain

{
    public class Product : BaseObject
    {
        private readonly ILazyLoader _lazyLoader;
        private Company createCompany;

        public Product()
        {

        }
        public Product(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }
        public string Name { get; set; }
        public string Captions { get; set; }
        public Company CreateCompany { get => _lazyLoader.Load(this, ref createCompany); set => createCompany = value; }

        public override void UpdatePropertys(BaseObject obj)
        {
        }
    }
}
