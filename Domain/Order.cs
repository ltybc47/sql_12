﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
  public class Order : BaseObject
  {

    public List<ElementOrder> products { get; set; }
    public Client Buyer { get; set; }

        public override void UpdatePropertys(BaseObject obj)
        {
            products = (obj as Order).products;
        }
    }

  public class ElementOrder : BaseObject
  {
    public Product product { get; set; }
    public int Count { get; set; }

        public override void UpdatePropertys(BaseObject obj)
        {
            product = (obj as ElementOrder).product;
        }
    }
}
