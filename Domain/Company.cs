﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
  public class Company : BaseObject
  {
    public Company()
    {
      CreateTime = DateTime.Now;
    }
    [Display(Name = "Название компании")]
    public string Name { get; set; }
    [Display(Name = "Описание")]
    public string Description { get; set; }
    public string Image { get; set; }
    [Display(Name = "Дата создания")]
    public DateTime CreateTime { get; set; }

    public override string ToString()
    {
      return Name + " " + CreateTime.ToShortDateString();
    }

        public override void UpdatePropertys(BaseObject obj)
        {
            this.Name = (obj as Company).Name;
        }
    }
}
