﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
  public class MyAttribute : Attribute // создаём собственный атрибут наследуясь от стандартного класса
  {
    public int Count { get; set; } // создаём своё свойство которое будет содержать атрибут
                                   // можно описать несколько свойств но для примера создаётся только одно
  }

}
