﻿namespace Domain
{
    public interface IBaseObject<T>
    {
        int GetId();
        abstract void UpdatePropertys(T obj);
    }
}
