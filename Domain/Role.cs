﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Role : BaseObject
    {
        public string Name { get; set; }
        [Column("RoleDesc")]
        public string Desc { get; set; }

        public override void UpdatePropertys(BaseObject obj)
        {
            Name = (obj as Role).Name;
            Desc = (obj as Role).Desc;
        }
    }
}
