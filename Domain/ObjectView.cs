﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
  interface ObjectView
  {
    Dictionary<string,object> GetDictionaryPropertys();
  }
}
