﻿using System.Linq;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure
{
  public static class DbContextExtentions
  {
    public static IQueryable<T> Set<T>(this DbContext _context)
    {
      return (IQueryable<T>)_context.GetType().GetMethod("Set").MakeGenericMethod(typeof(T)).Invoke(_context, null);
    }
    public static ProjectDbContext GetDbContext(string connectionString)
    {
      //      Установите 2 пакета
      //Microsoft.Extensions.Configuration.FileExtensions
      //Microsoft.Extensions.Configuration.Json

      var builder = new ConfigurationBuilder();
      // установка пути к текущему каталогу
      string baseDirectory = Directory.GetCurrentDirectory();
      builder.SetBasePath(baseDirectory);
      
      // получаем конфигурацию из файла appsettings.json
      builder.AddJsonFile("appsettings.json");
      // создаем конфигурацию
      IConfigurationRoot config = builder.Build();
      // получаем строку подключения
      var optionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>();
      var options = optionsBuilder
          .UseSqlServer(connectionString)
          .Options;

      return new ProjectDbContext(options);
    }
  }
}
