﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Infrastructure.Utils
{
  public static class PasswordHash
  {
    public static void GetPasswordHash(string password)
    {
      
      // generate a 128-bit salt using a secure PRNG
      byte[] salt = new byte[128 / 8];
      using (var rng = RandomNumberGenerator.Create())
      {
        rng.GetBytes(salt);
      }
      Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

      // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
      string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
          password: password,
          salt: salt,
          prf: KeyDerivationPrf.HMACSHA1,
          iterationCount: 10000,
          numBytesRequested: 256 / 8));
      Console.WriteLine($"Hashed: {hashed}");
    }
    
  }
  public class Hash
  {
    public static string Create(string value, string salt)
    {
      var valueBytes = KeyDerivation.Pbkdf2(
                          password: value,
                          salt: Encoding.UTF8.GetBytes(salt),
                          prf: KeyDerivationPrf.HMACSHA512,
                          iterationCount: 10000,
                          numBytesRequested: 256 / 8);

      return Convert.ToBase64String(valueBytes);
    }

    public static bool Validate(string value, string salt, string hash)
        => Create(value, salt) == hash;
  }
  public class Salt
  {
    public static string Create()
    {
      byte[] randomBytes = new byte[128 / 8];
      using (var generator = RandomNumberGenerator.Create())
      {
        generator.GetBytes(randomBytes);
        return Convert.ToBase64String(randomBytes);
      }
    }
  }


}
