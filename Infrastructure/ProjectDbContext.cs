﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Company> Companys { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Article> Article { get; set; }
        //public DbSet<Order> Order{ get; set; }


        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();   //    Если БД нету то он ее создаст
        }
    }
}
