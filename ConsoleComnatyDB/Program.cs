﻿using Infrastructure;
using System;
using Domain;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;

namespace ConsoleComnatyDB
{
  class Program
  {
    static void Main(string[] args)
    {

      //      Установите 2 пакета
      //Microsoft.Extensions.Configuration.FileExtensions
      //Microsoft.Extensions.Configuration.Json

      var builder = new ConfigurationBuilder();
      // установка пути к текущему каталогу
      builder.SetBasePath(Directory.GetCurrentDirectory());
      // получаем конфигурацию из файла appsettings.json
      builder.AddJsonFile("appsettings.json");
      // создаем конфигурацию
      var config = builder.Build();
      // получаем строку подключения
      string connectionString = config.GetConnectionString("DefaultConnection");

      var optionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>();
      var options = optionsBuilder
          .UseSqlServer(connectionString)
          .Options;

      using (ProjectDbContext db = new ProjectDbContext(options))
      {
        db.GetService<ILoggerFactory>().AddProvider(new MyLoggerProvider());

        //создаем два объекта User
        Company Otus = new Company { Name = "Otus", CreateTime = DateTime.Now };
        Company Epam = new Company { Name = "Epam", CreateTime = DateTime.Now };
        Company Sber = new Company { Name = "Sber", CreateTime = DateTime.Now };
        //Category cat1 = new Category { Name = "Main", ChildsCategory = new System.Collections.Generic.List<Category>() { new Category { Name = "child" } } };
        //db.Categorys.Add(cat1);
        db.Companys.AddRange(new Company[] { Otus, Epam, Sber });
        db.SaveChanges();
        Console.WriteLine("Объекты успешно сохранены");

        // получаем объекты из бд и выводим на консоль
        Console.WriteLine("Список объектов:");
        foreach (Company c in db.Companys.ToList())
        {
          Console.WriteLine($"{c.Id}.{c.Name}");
        }
      }
      Console.Read();
    }
  }
}
