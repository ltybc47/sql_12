﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Utils.Tests
{
  [TestClass()]
  public class PasswordHashTests
  {
    [TestMethod()]
    public void PasswordHashTest()
    {
      var message = "passw0rd";
      var salt = Salt.Create();
      var hash = Hash.Create(message, salt);

      // Act  
      var match = Hash.Validate(message, salt, hash);

      // Assert  
      Assert.IsTrue(match);

      //Assert.Fail();
    }

    [TestMethod()]
    public void Tampered_hash_does_not_matche_the_text()
    {
      // Arrange  
      var message = "passw0rd";
      var salt = Salt.Create();
      var hash = "blahblahblah";

      // Act  
      var match = Hash.Validate(message, salt, hash);

      // Assert  
      Assert.IsFalse(match);
    }

    [TestMethod()]
    public void Hash_of_two_different_messages_dont_match()
    {
      // Arrange  
      var message1 = "passw0rd";
      var message2 = "password";
      var salt = Salt.Create();

      // Act  
      var hash1 = Hash.Create(message1, salt);
      var hash2 = Hash.Create(message2, salt);

      // Assert  
      Assert.IsTrue(hash1 != hash2);
    }

  }
}