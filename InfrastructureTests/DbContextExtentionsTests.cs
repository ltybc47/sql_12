﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Infrastructure.Tests
{
    [TestClass()]
    public class DbContextExtentionsTests
    {
        private ProjectDbContext _db;
        [TestMethod()]
        public void SetTest()
        {
            _db = new ProjectDbContext();
            List<Company> listCompanys = new List<Company>();

            //listCompanys = _db.Companys.ToList();
            listCompanys = _db.Set<Company>().ToList();

            Console.WriteLine(string.Join("\n", listCompanys.Select(x => SerializatingFromReflection(x))));
            Assert.Fail();
        }

        [TestMethod()]
        public void CreateClientTest()
        {
            try
            {

                _db = new ProjectDbContext();
                Client client = new Client() { Name = "Li" };
                _db.Clients.Add(client);
                _db.SaveChanges();

                Console.WriteLine(string.Join("\n", _db.Clients.ToList().Select(x => SerializatingFromReflection(x))));
                //Assert.Fail();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                if (ex.InnerException != null)
                {
                    Exception exception = ex.InnerException;
                    Console.WriteLine(exception.Message);
                    Console.WriteLine(exception.Source);

                }
            }
        }
        [TestMethod()]
        public void CreateCategoryTest()
        {
            try
            {

                _db = new ProjectDbContext();
                Category mainCat = _db.Categorys.FirstOrDefault(x => x.Name.Equals("Для дачи"));
                if (mainCat != null)
                {
                    Category category = new Category() { Name = "Ведра", ParentCategory = mainCat };
                    _db.Categorys.Add(category);
                    _db.SaveChanges();

                    Console.WriteLine(string.Join("\n", _db.Categorys.ToList().Select(x => x.Name + " " + x.ParentCategory?.Name)));

                }
                else
                {
                    Assert.Fail();
                }
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                if (ex.InnerException != null)
                {
                    Exception exception = ex.InnerException;
                    Console.WriteLine(exception.Message);
                    Console.WriteLine(exception.Source);

                }
                Assert.Fail();
            }
        }
        private static string SerializatingFromReflection(object d)
        {
            string methods = String.Join("\n", d.GetType().GetMethods().Select(x => x.Name.ToString()).ToList());

            string fields = String.Join("\n", d.GetType().GetFields().Select(x => x.Name.ToString() + " - " + x.GetValue(d).ToString()).ToList());
            string property = String.Join("\n", d.GetType().GetProperties().Select(x => x.Name.ToString()));
            string attributes = String.Join("\n", d.GetType().GetProperties().Select(x => (x.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute)?.Name));

            return "Methods\n" + methods + "\n---\nFields\n" + fields + "\n---\nPropertys\n" + property + "\n---\nAttributes\n" + attributes;
        }
    }


}